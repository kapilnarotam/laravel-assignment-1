<?php

use \App\DataTables\UserDataTable;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/row_details', function () {
    return view('row_details');
})->middleware('auth');

Route::get('/builder', function (UserDataTable $dataTable) {
    return $dataTable->render('builder');
})->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('users', 'HomeController@getUsers')->name('get.users');

Auth::routes();


