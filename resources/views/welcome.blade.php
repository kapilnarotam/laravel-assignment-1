<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Normal DataTable</a>
                        <a href="{{ url('/builder') }}">YAJRA HTML Builder</a>
                        <a href="{{ url('/row_details') }}">Row Details</a>
                        <a href="{{ url('/telescope') }}">Audit trail</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Items Done
                </div>

                <div class="links">
                    <h2>Normal Datatables</h2>
                    <ul>
                        <li>Pagination</li>
                        <li>Search</li>
                        <li>Column Sort</li>
                    </ul>
                    <h2>YAJRA Datatables</h2>
                    <ul>
                        <li>Pagination</li>
                        <li>Search</li>
                        <li>Column Sort</li>
                        <li>Columns Search</li>
                        <li>CSV Export</li>
                    </ul>
                    <h2>YAJRA Datatables Manual HTML</h2>
                    <ul>
                        <li>Pagination</li>
                        <li>Search</li>
                        <li>Column Sort</li>
                        <li>Columns Search</li>
                        <li>Row Details</li>
                    </ul>
                    <h2>Audit Trail</h2>
                </div>
            </div>
        </div>
    </body>
</html>
