  @extends('layouts.app2')
@section('title', 'HTML')
  @section('content')
      <div class="card-body">
          <table class="table table-bordered" id="customers-table">
              <thead>
                  <tr>
                      <th></th>
                      <th>Id</th>
                      <th>First name</th>
                      <th>Email</th>
                      <th>Created At</th>
                      <th>Updated At</th>
                  </tr>
              </thead>
              <tfoot>
                  <tr>
                      <td style="visibility:hidden"></td>
                      <td class="searchable"></td>
                      <td class="searchable"></td>
                      <td class="searchable"></td>
                      <td class="searchable"></td>
                      <td class="searchable"></td>
                  </tr>
              </tfoot>
          </table>
      </div>
  @endsection

  @section('javascript')

      <script id="details-template" type="text/x-handlebars-template">
          @verbatim
              <table class="table">
                  <tr>
                      <td>Full name:</td>
                      <td>{{name}}</td>
                  </tr>
                  <tr>
                      <td>Email:</td>
                      <td>{{email}}</td>
                  </tr>
              </table>
          @endverbatim
      </script>

      <script>
            var template = Handlebars.compile($("#details-template").html());
            var table = $('#customers-table').DataTable({
              processing: true,
              serverSide: true,
              ajax: '{!! route('get.users') !!}',
              columns: [
                {
                  "className":      'details-control',
                  "orderable":      false,
                  "searchable":     false,
                  "data":           null,
                  "defaultContent": '<button>+</button>'
                },
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' },
              ],
              order: [[1, 'asc']],
              initComplete: function () {
            this.api().columns().every(function () {
              var column = this;

              //example for removing search field
              if (column.footer().className == 'searchable') {
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                .keyup(function () {
                  column.search($(this).val(), false, false, true).draw();
                });
              }
            });
          }
            });

            $('#customers-table tbody').on('click', 'td.details-control', function () {
              var tr = $(this).closest('tr');
              var row = table.row( tr );

              if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
              }
              else {
                // Open this row
                row.child( template(row.data()) ).show();
                tr.addClass('shown');
              }


            });

      </script>
  @endsection
