# DataTables assignment
### **[/home]()** makes use of DataTables.net with a manually created form

---

Features:

* main search box
* column sorting
* Pagination
* authentication

### **[/builder]()** makes use of yajrabox and html builder

Features:

* main search box
* column sorting
* Pagination
* search under each column
* CSV export
* authentication
### **[/row_details]()** makes use of yajrabox and html manual tables

Features:

* main search box
* column sorting
* Pagination
* search under each column
* authentication
* detailed row

### **[/telescope]()** For audit trail

### Database settings

mysql database, named: laravelassignment1 (included)

### LOGIN Instructions.
username: kaps@email.com
pass: password12345



